import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

function checkPassFile(passFile, req) {
  let res;
  if (
    JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter((item) => {
      if (item.filename === req.body.filename) {
        res = item;
        return true;
      }
    }).length > 0
  ) {
    return res;
  } else {
    return false;
  }
}
export function createFile(req, res, next) {
  try {
    if (!req.body.content || !req.body.filename) {
      res.status(400).json({
        message: `Please specify '${
          req.body.content ? 'filename' : 'content'
        }' parameter`,
      });
    } else {
      const filePath = path.join(__dirname, 'files', req.body.filename);
      const passFile = path.join(__dirname, 'files', 'pass.json');
      const newObj = {
        pass: req.query.pass || '',
        filename: req.body.filename,
      };
      if (newObj.pass === '') {
        fs.writeFile(filePath, req.body.content, (err) => {
          res.status(200).json({
            message: 'File created successfully',
          });
        });
      } else {
        if (!fs.existsSync(passFile)) {
          fs.writeFileSync(passFile, JSON.stringify({ elements: [newObj] }));
        } else {
          if (checkPassFile(passFile, req)) {
            fs.writeFileSync(
              passFile,
              JSON.stringify({
                elements: JSON.parse(
                  fs.readFileSync(passFile, 'utf-8')
                ).elements.map((item) => {
                  if (item.filename === req.body.filename) {
                    item.pass = req.query.pass || '';
                  }
                  return item;
                }),
              })
            );
          } else {
            fs.writeFileSync(
              passFile,
              JSON.stringify({
                elements: JSON.parse(
                  fs.readFileSync(passFile, 'utf-8')
                ).elements.concat(newObj),
              })
            );
            fs.writeFile(filePath, req.body.content, (err) => {
              res.status(200).json({
                message: 'File created successfully',
              });
            });
          }
        }
        if (req.query.pass) {
          fs.writeFile(filePath, req.body.content, (err) => {
            res.status(200).json({
              message: 'File created successfully',
            });
          });
        } else {
          const result = checkPassFile(passFile, req);
          if (result.pass === '') {
            fs.writeFile(filePath, req.body.content, (err) => {
              res.status(200).json({
                message: 'File created successfully',
              });
            });
          } else if (res) {
            throw new Error();
          }
        }
      }
    }
  } catch (error) {
    next(err);
  }
}

export function getFiles(req, res, next) {
  try {
    const dirPath = path.join(__dirname, 'files');
    const findDir = fs.opendirSync(dirPath);
    if (!findDir) {
      res.status(400).json({
        message: 'Client error',
      });
    } else {
      res
        .status(200)
        .json({ message: 'Success', files: fs.readdirSync(dirPath) });
    }
  } catch (error) {
    next(error);
  }
}

export const getFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, 'files', req.params.filename);
    const passFile = path.join(__dirname, 'files', 'pass.json');
    if (!fs.existsSync(filePath)) {
      res.status(400).json({
        message: `No file with '${path.basename(filePath)}' filename found`,
      });
    } else {
      let filePass;
      if (fs.existsSync(passFile)) {
        filePass = JSON.parse(fs.readFileSync(passFile, 'utf8')).elements.find(
          (item) => {
            return item.filename === req.params.filename;
          }
        );
      }
      if (filePass) {
        if (req.query.pass) {
          if (filePass.pass === req.query.pass) {
            res.status(200).json({
              message: 'Success',
              filename: req.params.filename,
              content: fs.readFileSync(filePath, 'utf8'),
              extension: path.extname(filePath).slice(1),
              uploadedDate: fs.statSync(filePath).birthtime,
            });
          } else {
            throw new Error();
          }
        } else {
          throw new Error();
        }
      } else {
        if (req.query.pass) {
          throw new Error();
        }
        res.status(200).json({
          message: 'Success',
          filename: req.params.filename,
          content: fs.readFileSync(filePath, 'utf8'),
          extension: path.extname(filePath).slice(1),
          uploadedDate: fs.statSync(filePath).birthtime,
        });
      }
    }
  } catch (error) {
    next(error);
  }
};

export const deleteFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, 'files', req.params.filename);
    const passFile = path.join(__dirname, 'files', 'pass.json');
    if (!fs.existsSync(filePath)) {
      throw new Error();
    } else {
      if (req.query.pass) {
        if (
          JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
            (item) => {
              if (
                item.filename === req.params.filename &&
                item.pass === req.query.pass
              ) {
                return true;
              }
            }
          ).length > 0
        ) {
          fs.writeFileSync(
            passFile,
            JSON.stringify({
              elements: JSON.parse(
                fs.readFileSync(passFile, 'utf-8')
              ).elements.filter((item) => {
                return item.filename !== req.params.filename;
              }),
            })
          );
          fs.unlinkSync(filePath);
          res.status(200).json({
            message: 'File was deleted!',
            filename: req.params.filename,
            extension: path.extname(filePath).slice(1),
          });
        } else {
          throw new Error();
        }
      } else if (
        JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
          (item) => {
            return item.filename === req.params.filename;
          }
        ).length > 0
      ) {
        throw new Error();
      } else {
        fs.unlinkSync(filePath);
        res.status(200).json({
          message: 'File was deleted!',
          filename: req.params.filename,
          extension: path.extname(filePath).slice(1),
        });
      }
    }
  } catch (error) {
    next(error);
  }
};

export const editFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, 'files', req.body.filename);
    const passFile = path.join(__dirname, 'files', 'pass.json');
    if (!fs.existsSync(filePath)) {
      res.status(400).json({
        message: `No file with '${req.body.filename}' filename found`,
      });
    } else {
      let filePass;
      if (
        (filePass = JSON.parse(fs.readFileSync(passFile, 'utf8')).elements.find(
          (item) => {
            return item.filename === req.body.filename;
          }
        ))
      ) {
        if (req.query.pass && req.query.pass === filePass.pass) {
          fs.writeFileSync(filePath, req.body.content);
          res.status(200).json({
            message: 'File was updated!',
            filename: req.body.filename,
            newContent: req.body.content,
          });
        } else {
          throw new Error();
        }
      } else {
        fs.writeFileSync(filePath, req.body.content);
        res.status(200).json({
          message: 'File was updated!',
          filename: req.body.filename,
          newContent: req.body.content,
        });
      }
    }
  } catch (error) {
    next(error);
  }
};
