import fs from 'fs';
import express from 'express';
import morgan from 'morgan';
const app = express();

import router from './filesRouter.js';
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', router);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  res.status(500).send({ message: 'Server error' });
}
